<label>[[weather:adminGeotext]]:</label>
<input type="text" class="form-control" name="geotext" placeholder="chicago, il" />
<label>[[weather:adminForecastlength]]:</label>
<input type="text" class="form-control" name="forecastlength" placeholder="[0-9]" />
<label>[[weather:adminCacheduration]]:</label>
<input type="text" class="form-control" name="cacheduration" placeholder="30" />
<div class="checkbox">
	<label>
		<input type="checkbox" name="yahoologowhite" />&nbsp;[[weather:adminYahoologowhite]]
	</label>
</div>