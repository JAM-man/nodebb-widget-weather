<!-- [[parserss:adminAddress]] -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.9/css/weather-icons.css">
<!-- IF weatherlocation.city -->
<div class="row" id="weatherLocationRow">
	<div class="col-sm-12 text-right">
		<!-- IF weather.reflink -->
		<a href="{weather.reflink}" target="_blank">{weatherlocation.city}&nbsp;<i class="glyphicon glyphicon-map-marker"></i></a>
		<!-- ELSE -->
		{weatherlocation.city}&nbsp;<i class="glyphicon glyphicon-map-marker"></i>
		<!-- ENDIF weather.reflink -->
	</div>
</div>
<!-- ENDIF weatherlocation.city -->
<!-- IF weather.condition -->
<div class="row row-eq-height" id="weatherTodayRow">
	<!-- IF weather.condition.temp -->
	<div class="col-sm-4 text-center" id="weatherActTempCol">
		{weather.condition.temp}°
	</div>
	<!-- ENDIF weather.condition.temp -->
	<div class="col-sm-8" id="weatherActCondCol">
		<!-- IF weather.condition.code -->
		<div class="row" id="weatherActCondRow">
			<div class="col-sm-1">
				<i class="wi wi-yahoo-{weather.condition.code}"></i>
			</div>
			<div class="col-sm-9">[[weather:{weather.condition.code}]]</div>
		</div>
		<!-- ELSE -->
		<!-- IF weather.condition.text -->	
		<div class="row" id="weatherActCondRow">
			<div class="col-sm-offset-1 col-sm-9">{weather.condition.text}</div>
		</div>
		<!-- ENDIF weather.condition.text -->
		<!-- ENDIF weather.condition.code -->
		<div class="row" id="weatherTodayTempRow">
			<!-- IF weather.condition.high -->
			<div class="col-sm-1 hidden-xs"><i class="wi wi-direction-up"></i></div>
			<div class="col-sm-2"><i class="wi wi-direction-up visible-xs">&nbsp;&nbsp;</i>{weather.condition.high}°</div>
			<!-- ENDIF weather.condition.high -->
			<!-- IF weather.condition.low -->
			<div class="col-sm-1 hidden-xs"><i class="wi wi-direction-down"></i></div>
			<div class="col-sm-2"><i class="wi wi-direction-down visible-xs">&nbsp;&nbsp;</i>{weather.condition.low}°</div>
			<!-- ENDIF weather.condition.low -->
		</div>
	</div>
</div>
<!-- ENDIF weather.condition -->
<!-- IF weather.forecast -->
<div class="row" id="weatherForecastRow">
	<div class="col-sm-12">
		<table class="table">
			<tbody>
				<!-- BEGIN weather.forecast -->
				<tr>
					<!-- IF weather.forecast.day -->
					<td>[[weather:{weather.forecast.day}]]</td>
					<!-- ENDIF weather.forecast.day -->
					<!-- IF weather.forecast.code -->
					<td><i class="wi wi-yahoo-{weather.forecast.code}"></i></td>
					<td>[[weather:{weather.forecast.code}]]</td>
					<!-- ELSE -->
					<!-- IF weather.forecast.text -->
					<td>{weather.forecast.text}</td>
					<!-- ENDIF weather.forecast.text -->
					<!-- ENDIF weather.forecast.code -->
					<!-- IF weather.forecast.high -->
					<!-- IF weather.forecast.low -->
					<td>{weather.forecast.high}°/{weather.forecast.low}°</td>
					<!-- ELSE -->
					<td>{weather.forecast.high}°</td>
					<!-- ENDIF weather.forecast.low -->
					<!-- ENDIF weather.forecast.high -->
				</tr>
				<!-- END weather.forecast -->
			</tbody>
		</table>
	</div>
</div>
<!-- ENDIF weather.forecast -->
<!-- IF weatherimage.link -->
<div class="row" id="weatherYahooRow">
	<div class="col-sm-12"><a href="{weatherimage.link}" target="_blank"><img class="img-responsive pull-right" src="https://poweredby.yahoo.com/{weatherlogo.name}.png" width="134" height="29"/></a></div>
</div>
<!-- ENDIF weatherimage.link -->