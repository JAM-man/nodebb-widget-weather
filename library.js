'use strict';

Date.minutesBetween = function(fromdate, todate) {
	var diff = todate - fromdate;
	return Math.floor(diff/60000);
};

var async =  module.parent.require('async');

var db = module.parent.require('./database');

var YQL = require('yql');

var app;

var Widget = module.exports;

Widget.init = function(params, callback) {
	app = params.app;

	callback();
};

Widget.renderParseWeatherWidget = function(widget, callback) {
	var geotext = widget.data.geotext;

	if(!geotext) {
		return callback(null, widget);
	}
	var forecastlength = getNaturalNumber(widget.data.forecastlength, 0) + 1;
	var cacheduration = getNaturalNumber(widget.data.cacheduration, 30);

	var logo = "purple";
	if(widget.data.yahoologowhite) {
		logo = "white";
	}

	async.waterfall([
		function(next) {
			getWeatherInformation(geotext, cacheduration, next);
		},
		function(data, next) {
			setForecast(data, forecastlength, next);
		},
		function(data, next) {
			setRefLink(data, next);
		},
		function(data, next) {
			var weatheritem = {}, weatherlocation = {}, weatherimage = {};

			if(hasOwnNestedProperty(data, 'query.results.channel')) {
				weatheritem = data.query.results.channel.item;
				weatherlocation = data.query.results.channel.location;
				weatherimage = data.query.results.channel.image;
			}

			app.render('widget/weather', {
				weather: weatheritem,
				weatherlocation: weatherlocation,
				weatherimage: weatherimage,
				weatherlogo: {name: logo}
			}, next);
		},
		function(html, next) {
			widget.html = html;
			next(null, widget);
		}
	], callback);
};

function getNaturalNumber(numitem, defaultnum) {
	return numitem == parseInt(numitem, 10) && numitem >= 0 ? parseInt(numitem) : defaultnum;
}

function hasOwnNestedProperty(obj, propertyPath) {
	if(!propertyPath) {
		return false;
	}

	var properties = propertyPath.split('.');

	for(var i = 0; i < properties.length; i++) {
		var prop = properties[i];

		if(!obj || !obj.hasOwnProperty(prop)) {
			return false;
		} else {
			obj = obj[prop];
		}
	}

	return true;
};

function setRefLink(data, callback) {
	if(hasOwnNestedProperty(data, 'query.results.channel.item.link') && !data.query.results.channel.item.reflink) {
		var linkArr = data.query.results.channel.item.link.split("*");
		if(linkArr.length >= 1 && linkArr[linkArr.length - 1].startsWith("http")) {
			data.query.results.channel.item.reflink=linkArr[linkArr.length - 1];
		}
	}
	callback(null, data);
}

function setForecast(data, forecastlength, callback) {
	if(hasOwnNestedProperty(data, 'query.results.channel.item') && Array.isArray(data.query.results.channel.item.forecast) && !data.query.results.channel.item.forecastbuild) {
		forecastlength = Math.min(data.query.results.channel.item.forecast.length, forecastlength);
		var forecastarray = [];

		if(data.query.results.channel.item.forecast.length >= 1) {
			data.query.results.channel.item.condition.high = data.query.results.channel.item.forecast[0].high;
			data.query.results.channel.item.condition.low = data.query.results.channel.item.forecast[0].low;
			data.query.results.channel.item.condition.day = data.query.results.channel.item.forecast[0].day;
		}

		for(var i = 1; i<forecastlength; i++) {
			forecastarray.push(data.query.results.channel.item.forecast[i]);
		}

		data.query.results.channel.item.forecast = forecastarray;
		data.query.results.channel.item.forecastbuild = true;
	}
	callback(null, data);
}

function getWeatherInformation(geotext, cacheduration, callback) {
	async.waterfall([
		function(next) {
			db.get("widget:weather", next);
		},
		function(data, next) {
			if(data && data.query && data.query.created) {
				var then = new Date(data.query.created);
				var now = Date.now();
				if(Date.minutesBetween(then, now) <= cacheduration) {
					next(null, data);
				} else {
					db.delete("widget:weather", function(err) { });
					next(null, null);
				}
			} else {
				next(null, null);
			}
		},
		function(data, next) {
			if(data === null) {
				var query = new YQL('select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=@locationtext) and u=\'c\'').setParam('locationtext', geotext);
				async.waterfall([
					function(innernext) {
						query.exec(innernext);
					},
					function(data, innernext) {
						db.set("widget:weather", data, function(err) { });
						innernext(null, data);
					}
				], next);
			} else {
				next(null, data);
			}
		}
	], callback);
}

Widget.defineWidgets = function(widgets, callback) {
	async.waterfall([
		function(next) {
			app.render('admin/weather', {}, function(err, html) {
				widgets.push({
					widget: "weather",
					name: "Weather-Widget",
					description: "Widget to display Weather.",
					content: html
				});
				next(err, widgets);
			});
		}
	], callback);
};